
## Spring Boot Application Requirements
1. JDK 1.8 or later
2. Maven 3.2+

---

## How to run the Spring Boot Application
1. Run this in the command line **git clone https://bitbucket.org/mkadusale/creditcard.git**
2. Then run this command **mvn spring-boot:run** in the root folder(ex. D:\workspace\creditcard) of the application where the pom.xml is located

---

## How to validate a credit card number
1. After running the application, open a browser
3. Copy this URL **http://localhost:8080/creditcard/verify/378282246310005**
4. The syntax of the URL **http://localhost:8080/creditcard/verify/****place-credit-card-number-here**
5. If the credit card number is valid it will return a JSON String **{"result":"valid"}**, but if the credit card number is invalid it will return this **{"result":"invalid"}**

---


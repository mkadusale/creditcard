package com.creditcard.creditcard.service;

import org.springframework.stereotype.Service;


@Service
public class CreditCardService {

    public boolean isValid(String ccNum) {
        boolean isValid = true;

        try {
            Long cardNumber = Long.parseLong(ccNum);
            
            if (ccNum.length()<13 || ccNum.length()>16) {
                isValid = false;
            }

            if (isValid==true && isValidCardType(ccNum)==false){
                isValid = false;
            }

            if (isValid==true && isValidCCNumber(ccNum)==false){
                isValid = false;
            }

        } 
        catch (NumberFormatException e) {
            isValid = false;
        }
        catch (Exception e) {
            isValid = false;
        }

        return isValid;
    }


    public boolean isValidCardType(String number) {
        boolean isValid = false;
        if ( isValid==false && (number.substring(0,2).equals("34") || number.substring(0,2).equals("37")) && number.length()==15 ) {
            isValid = true;
        }

        if ( isValid==false && number.substring(0,4).equals("6011") && number.length()==16 ) {
            isValid = true;
        }

        int firstNumbers = Integer.parseInt(number.substring(0,2));
        if ( isValid==false && firstNumbers>50 && firstNumbers<56 && number.length()==16 ) {
            isValid = true;
        }

        if ( isValid==false && number.substring(0,1).equals("4") && (number.length()==13 || number.length()==16) ) {
            isValid = true;
        }

        return isValid;
    }


    public boolean isValidCCNumber(String n) {
        boolean isValid = true;
        try {
            boolean isNext = false; 
            int numDigits = n.length() - 1;
            int totalSum = 0;
            int num = 0;

            for (int index=numDigits; index>=0; index--) {
                if (isNext == true){
                    num = (Integer.parseInt(n.substring(index, index+1))) * 2;
                    if (num>9) {
                        String strNum = Integer.toString(num);
                        int firstNum = Integer.parseInt(strNum.substring(0,1));
                        int secondNum = Integer.parseInt(strNum.substring(1,2));
                        totalSum += (firstNum + secondNum);
                    } else {
                        totalSum += num;
                    }
                } else {
                    num = Integer.parseInt(n.substring(index, index+1 ));
                    totalSum += num;
                }

                isNext = !isNext; 
            }

            if ((totalSum % 10) != 0) {
                isValid = false;
            }
        }
        catch (Exception e) {
          e.printStackTrace();
          isValid = false;
        }


        return isValid;
    }

        

}
package com.creditcard.creditcard.controller;

import java.util.HashMap;
import java.util.Map;

import com.creditcard.creditcard.service.CreditCardService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController {
    @Autowired
    CreditCardService ccSvc;

    @RequestMapping(value = "/verify/{ccNum}", method = RequestMethod.GET)
    public String verify(@PathVariable("ccNum") String ccNum) {
        Map<String, String> payload = new HashMap<>();
        String result = "invalid";
        String json ="";

        if (ccSvc.isValid(ccNum)) {
            result = "valid";
        }

        payload.put("result", result);
        
        try {
            json = new ObjectMapper().writeValueAsString(payload);
        } catch (JsonProcessingException e) {
        }
        return json;        
    }

}
